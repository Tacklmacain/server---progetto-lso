#include "utilitySocket.h"
#include "utilityThread.h"
#include "list.h"

/* ************************************************************************ */

int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        printError("Errore: Numero di argomenti non valido\n");
        exit(-1);
    }

    char* nomeFile = argv[1];
    char* serverPort = argv[2];
    char* printStr = calloc(MAX_STRING, sizeof(char));

    // Apro il file in sola lettura, se non esiste fallisce
    int configFileDescriptor = open(nomeFile, O_RDONLY);
    if(configFileDescriptor < 0)
    {
        perror("Errore apertura file configurazione");
    }

    /* Dichiaro alcuni attributi del server, come il suo ip, sid (Server ID),
     * socketDescriptor ed infine il numero di server a cui connettersi */
    char* serverIp = malloc(sizeof(char) * (SIZE_OF_IP + 1));
    int tmpSid = -1, tmpSocketDescriptor = -1, row = 0;

    /* Inizio a scorrere il file per verificare se la porta inserita e valida e contare
     * il numero di server del file di configurazione */
    int endOfFile = lseek(configFileDescriptor, 0, SEEK_END);
    lseek(configFileDescriptor, 0, SEEK_SET);
    while(lseek(configFileDescriptor, 0, SEEK_CUR) < endOfFile || tmpSid == -1)
    {
        char currentIp[SIZE_OF_IP + 1];
        char currentPort[SIZE_OF_PORT + 1];

        /* Gestisco la possibilità che getIpAt mi torni errori, qui non mi serve un mutex perché
         * in questo punto del codice non esistono thread */
        if(getIpAt(row, configFileDescriptor, currentIp, currentPort) < 0)
        {
            free(serverIp);
            close(configFileDescriptor);
            printError("Errore: La porta inserita non esiste nel file di configurazione scelto oppure il file non "
                           "rispetta la forma xxx.yyy.zzz.www:pppp, inserire una porta valida o cambiare file.\n");
            exit(-1);
        }

        // Verifico se la porta corrente corrisponde a quella che sto cercando, se è così allora creo il socket
        if(strcmp(serverPort, currentPort) == 0)
        {
            tmpSocketDescriptor = createServerSocket(currentIp, atoi(currentPort), QUEUE_SIZE);

            // Se la creazione del socket è fallita esco con errore
            if (tmpSocketDescriptor < 0)
            {
                free(serverIp);
                close(configFileDescriptor);
                perror("Errore durante la creazione del server");
                exit(-1);
            }
            else
            {
                // Altrimenti salvo i dati
                strcpy(serverIp, currentIp);
                tmpSid = row;

                sprintf(printStr, "Il mio SID %d, ho ottenuto l'indirizzo %s:%s\n", tmpSid, serverIp, serverPort);
                printStr[strlen(printStr)] = '\0';
                printToMonitor(printStr);
            }
        }

        // Mi sposto alla prossima riga indipendentemente se l'ho trovato o meno
        row++;
    }

    /* Creo una variabile per memorizzare il numero di server totali, coincide con
     * l'ultima riga visitata, il sid, socketDescriptor come costanti */
    const int totalServers = row;
    const int sid = tmpSid;
    const int socketDescriptor = tmpSocketDescriptor;

    /* Avvio i thread che si occuperanno di connettersi ai rimanenti servers
     * dichiaro l'array contenente tutti i server ma il tid con indice sid
     * lo imposto a -1 per ignorarlo */
    pthread_t* threads = malloc(sizeof(pthread_t) * totalServers);
    pthread_mutex_t configMutex = PTHREAD_MUTEX_INITIALIZER;
    threads[sid] = (pthread_t) -1;

    // Ciclo sul numero di server su cui connettermi creando vari thread
    for (int j = 0; j < totalServers; ++j)
    {
        // Ciclo solo se j != sid in modo da non connettermi a me stesso
        if(j != sid)
        {
            // Delego un thread la connessione con questo indirizzo
            tConnect* tConnectStruct = malloc(sizeof(tConnect));
            tConnectStruct->configFile = &configFileDescriptor;
            tConnectStruct->configMutex = &configMutex;
            tConnectStruct->row = j;
            tConnectStruct->sid = sid;
            pthread_create((pthread_t *) &threads[j], NULL, &threadConnecter, tConnectStruct);
        }
    }

    /* Faccio la join sui vari thread per prendere i vari socket di scrittura
     * verso gli altri server, alloco anche il server corrente ma inserisco
     * il valore fittizio -1 a quella locazione*/
    sprintf(printStr, "Procedo a connettermi agli altri %d server...\n", totalServers - 1);
    printStr[strlen(printStr)] = '\0';
    printToMonitor(printStr);

    int* writeSockets = malloc(sizeof(int) * totalServers);
    writeSockets[sid] = -1;

    for (int k = 0; k < totalServers; ++k)
    {
        // Evito di fare la join per me stesso
        if(k != sid)
        {
            int* ret;
            pthread_join(threads[k], (void*) &ret);

            // Se il valore di ritorno è < 0 è fallito un thread
            if(*ret < 0)
            {
                free(serverIp);
                free(threads);
                close(configFileDescriptor);
                close(socketDescriptor);
                perror("Errore con la connessione verso un server");
                exit(-1);
            }

            writeSockets[k] = *ret;
            free(ret);
        }
    }

    /* Arrivato qui sono state inviate tutte le richieste, procedo ad accettarle in
     * modo da poter ricevere messaggi dagli altri server */
    int* readSockets = malloc(sizeof(int) * totalServers);
    readSockets[sid] = -1;

    // Ciclo finché ho server mancanti
    for (int i = 0; i < totalServers; ++i)
    {
        // Non posso accettare me stesso
        if(i != sid)
        {
            int tmp = accept(socketDescriptor, NULL, NULL);

            // Imposto il socket non bloccante per il polling
            setNonBlocking(tmp, true);

            /* Leggo dal socket il sid dell'altro server per posizionarlo
             * in modo corretto nell'array, siccome il socket è non bloccante
             * non so quando mi arriverà l'intero e devo ciclare */
            int position;
            while(receiveInt(tmp, &position) != 0)
            {
                sleep(1);
            }

            // Posiziono correttamente il socket nel suo indice e lo imposto non bloccante
            readSockets[position] = tmp;
            sprintf(printStr, "Ho accettato la richiesta del server %d\n", i);
            printStr[strlen(printStr)] = '\0';
            printToMonitor(printStr);
        }
    }

    // Dichiaro la lista dinamica del Server
    List* serverList = createList();

    // Dichiaro i mutex per i 2 array dei risultati e per i socket di scrittura
    pthread_mutex_t writeSocketMutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t resultStoreMutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t resultSearchMutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

    // Dichiaro i mutex client e server
    pthread_mutex_t clientMutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t serverMutex = PTHREAD_MUTEX_INITIALIZER;

    // Array di risultati, 1 se successo, -1 se fallimento, 0 se vuoto
    int* resultStore = calloc(totalServers, sizeof(int));
    int resultStoreIndex = 0;

    // Array di risultati ricerca, stringa se ha successo, costante NOT_FOUND se fallimento, NULL se vuoto
    char** resultSearch = malloc(sizeof(char*) * totalServers);
    int resultSearchIndex = 0;

    // L'array dei risultati è vuoto, faccio puntare ogni locazione a NULL
    for (int l = 0; l < totalServers; ++l) {
        resultSearch[l] = NULL;
    }

    // Dichiaro l'handler dei messaggi provenienti da altri server
    tRequest handler;
    handler.sid = sid;
    handler.writeSockets = writeSockets;
    handler.writeMutex = &writeSocketMutex;
    handler.readSockets = readSockets;
    handler.numServer = totalServers;
    handler.serverList = serverList;
    handler.resultSearch = resultSearch;
    handler.resultSearchMutex = &resultSearchMutex;
    handler.resultSearchIndex = &resultSearchIndex;
    handler.resultStore = resultStore;
    handler.resultStoreMutex = &resultStoreMutex;
    handler.resultStoreIndex = &resultStoreIndex;
    handler.condVar = &cond;
    handler.serverMutex = &serverMutex;

    int handlerTid;
    pthread_create((pthread_t *) &handlerTid, NULL, &threadReadHandler, &handler);

    // Installazione dell'handler per SIGINT
    signal(SIGINT, sigHand);

    // Prendo le variabili globali e le assegno
    extern int* globalWriteSock;
    extern pthread_mutex_t* globalWriteMux;
    extern int globalServerNumber;
    extern int globalSID;

    globalWriteSock = writeSockets;
    globalWriteMux = &writeSocketMutex;
    globalServerNumber = totalServers;
    globalSID = sid;

    // Main loop del server, attende chiamate e poi delega ad un thread la sua gestione
    int exitCode = 0;
    printToMonitor("Fase di inizializzazione terminata, in attesa di richieste dal client...\n");
    while(true)
    {
        // Il socket del server è bloccante, quindi sono in attesa di effettuare accept
        int newSocket = accept(socketDescriptor, NULL, NULL);

        // Gestisco un eventuale caso di errore della accept uscendo dal main loop
        if(newSocket < 0)
        {
            perror("Errore accept dal client");
            exitCode = -1;
            break;
        }

        // Creo il nuovo thread che si occuperà della richiesta client
        tRequest* request = malloc(sizeof(tRequest));
        request->sid = sid;
        request->clientSocket = newSocket;
        request->writeSockets = writeSockets;
        request->writeMutex = &writeSocketMutex;
        request->readSockets = readSockets;
        request->numServer = totalServers;
        request->serverList = serverList;
        request->resultSearch = resultSearch;
        request->resultSearchMutex = &resultSearchMutex;
        request->resultStoreIndex = &resultStoreIndex;
        request->resultStore = resultStore;
        request->resultStoreMutex = &resultStoreMutex;
        request->resultSearchIndex = &resultSearchIndex;
        request->condVar = &cond;
        request->clientMutex = &clientMutex;

        int tid;
        pthread_create((pthread_t *) &tid, NULL, &threadClientRequest, request);
    }

    // Chiusura di socket, file e free di memoria allocata dinamicamente
    for (int i = 0; i < totalServers; ++i)
    {
        if(i != sid)
        {
            close(writeSockets[i]);
            close(readSockets[i]);
        }
    }
    close(configFileDescriptor);

    destructList(serverList);
    free(resultSearch);
    free(resultStore);
    free(readSockets);
    free(writeSockets);
    free(threads);
    free(serverIp);
    free(printStr);
    return exitCode;
}