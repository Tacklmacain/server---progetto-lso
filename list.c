#include "list.h"

/* ************************************************************************ */

List* createList()
{
    List* newList = malloc(sizeof(List));
    newList->dim = 0;
    newList->head = NULL;
    newList->mutex = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(newList->mutex, NULL);

    return newList;
}

/**
 * Cerca la chiave data in ingresso nella lista, se la trova la assegna al parametro 'value'
 * @param list la lista
 * @param key la chiave che si intende cercare
 * @param value il valore corrispondente a quella chiave se esiste, invariato altrimenti (parametro di ritorno),
 * non deve essere allocato oppure genera memory leak
 * @return true se la chiave esiste, false altrimenti
 */
bool searchList(List* list, char* key, char** value)
{
    pthread_mutex_lock(list->mutex);
    Node* head = list->head;
    while(head != NULL)
    {
        if(strcmp(head->key, key) == 0)
        {
            *value = malloc(sizeof(char) * (strlen(head->value) + 1));
            strcpy(*value, head->value);
            pthread_mutex_unlock(list->mutex);
            return true;
        }
        head = head->next;
    }
    pthread_mutex_unlock(list->mutex);
    return false;
}

/**
 * Verifica se il parametro 'key' è nella lista, se non c'è aggiunge la coppia (key, value)
 * @param list la lista
 * @param key la chiave
 * @param value il valore corrispondente alla chiave
 * @return true se la coppia è stata aggiunta, false altrimenti
 */
bool storeList(List* list, char* key, char* value)
{    
    pthread_mutex_lock(list->mutex);
    Node* head = list->head;
    Node* prev = NULL;
    while(head != NULL)
    {
        if(strcmp(head->key, key) == 0)
        {
            pthread_mutex_unlock(list->mutex);
            return false;
        }
        prev = head;
        head = head->next;
    }

    Node* newNode = malloc(sizeof(Node));
    newNode->key = malloc(sizeof(char) * (strlen(key) + 1));
    strcpy(newNode->key, key);

    newNode->value = malloc(sizeof(char) * (strlen(value) + 1));
    strcpy(newNode->value, value);
    newNode->next = NULL;
    
    if(prev == NULL)
    {
        list->head = newNode;
    }
    else
    {
        prev->next = newNode;
    }
    
    (list->dim)++;

    pthread_mutex_unlock(list->mutex);
    return true;
}

/**
 * Cerca la chiave data in ingresso nella lista, se la trova ne sostituisce il valore
 * @param list la lista
 * @param key la chiave da cercare
 * @param value il nuovo valore da assegnare per quella chiave
 * @return true se la chiave esiste, false altrimenti
 */
bool corruptList(List* list, char* key, char* value)
{
    pthread_mutex_lock(list->mutex);
    Node* head = list->head;
    while(head != NULL)
    {
        if(strcmp(head->key, key) == 0)
        {
            free(head->value);
            head->value = malloc(sizeof(char) * (strlen(value) + 1));
            strcpy(head->value, value);
            pthread_mutex_unlock(list->mutex);
            return true;
        }
        head = head->next;
    }
    pthread_mutex_unlock(list->mutex);
    return false;
}

void destructList(List* list)
{
    pthread_mutex_lock(list->mutex);
    Node* head = list->head;
    Node* tmp;
    while(head != NULL)
    {
        tmp = head->next;
        free(head->key);
        free(head->value);
        free(head);
        head = tmp;
    }
    pthread_mutex_unlock(list->mutex);
    pthread_mutex_destroy(list->mutex);
    free(list->mutex);
    free(list);
}