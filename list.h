#ifndef LIST_H
#define LIST_H

/* ************************************************************************ */

// Librerie
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

/* ************************************************************************ */

// Struttura della lista
typedef struct Node Node;
struct Node
{
    char* key;
    char* value;
    Node* next;
};

typedef struct List
{
  Node* head;
  int dim;
  pthread_mutex_t* mutex;
} List;

/* ************************************************************************ */

// Funzioni
List* createList();
bool searchList(List*, char*, char**);
bool storeList(List*, char*, char*);
bool corruptList(List*, char*, char*);
void destructList(List*);

/* ************************************************************************ */

#endif