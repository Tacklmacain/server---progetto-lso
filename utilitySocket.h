#ifndef UTILITY_SOCKET_H
#define UTILITY_SOCKET_H

/* ************************************************************************ */

// Librerie
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

/* ************************************************************************ */

// Costanti
#define MAX_STRING 256

/* ************************************************************************ */

// Funzioni relative alla creazione e gestione di socket
int createServerSocket(char*, int, int);
int connectToOtherServer(char*, int);
bool setNonBlocking(int, bool);

// Funzioni relative alla lettura e scrittura su socket
int receiveInt(int, int*);
int sendInt(int, int);
int receiveString(int, char**, int);
int sendString(int, char*, int);

// Funzione di stampa a video (STDOUT) ed errori (STDERR)
void printToMonitor(char*);
void printError(char*);

// Funzioni relative ai segnali
void sigHand(int);

/* ************************************************************************ */

#endif