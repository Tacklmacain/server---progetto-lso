#include "utilityThread.h"
#include "utilitySocket.h"
#include "list.h"

/* ************************************************************************ */

/**
 * Restituisce l'ip e la porta alla riga corrispondente alla riga richiesta
 * @attention Se il fd è vuoto oppure position < endOfFile ritorna undefined error
 * @param row la riga del file che si intende leggere
 * @param fd il file descriptor
 * @param ip la stringa su cui andrà scritto l'ip (è un parametro di ritorno)
 * @param port la stringa su cui andrà scritta la porta (è un parametro di ritorno)
 * @return 0 se ha successo, -1 altrimenti
 */
int getIpAt(int row, int fd, char* ip, char* port)
{
    // Per ottenere la posizione devo moltiplicare per SIZE_OF_IP + ':' + SIZE_OF_PORT + '\n'
    int position = row * (SIZE_OF_PORT + SIZE_OF_IP + 2);

    // Salvo la fine del file
    int endOfFile = lseek(fd, 0, SEEK_END);

    // Se lseek su position mi ritorna qualcosa più grande della fine del file allora è fallito
    if(lseek(fd, position, SEEK_SET) > endOfFile)
    {
        return -1;
    }

    int index = 0;
    char currentChar;
    bool readZero = false;
    int zeroFoud = 0;

    /* Leggo l'indirizzo ip dal file facendo attenzioni agli 0 aggiunti per rispettare 
     * la forma xxx.yyy.zzz.www:pppp */
    for(int i = 0; i < SIZE_OF_IP; i++)
    {
        if(read(fd, &currentChar, 1) <= 0)
        {
            return -1;
        }

        // Controllo se currentChar è un numero diverso da 0
        if((int)currentChar >= 49 && (int)currentChar <= 57)
        {
            // Se ho trovato un numero leggo gli 0 successivi
            readZero = true;
        }
        else if(currentChar == '.')
        {
            // Se trovo un punto non devo più leggere gli zero
            readZero = false;
            zeroFoud = 0;
        }
        else if(currentChar == '0')
        {
            // L'unico 0 da leggere è il terzo 0 trovato
            if(zeroFoud < 2)
            {
                zeroFoud++;
            }
            else
            {
                readZero = true;
            }
        }

        /* Se ho trovato un numero, un punto, o uno 0 in posizione valida per rappresentare 
         * l'ip in forma contratta allora, lo leggo e lo aggiungo alla stringa del'ip */
        if(readZero || currentChar == '.')
        {
            ip[index] = currentChar;
            index++;
        }
    }
    // Aggiungo il carattere terminatore alla fine della stringa
    ip[index] = '\0';

    // Salto di 1 posizione per non leggere ':'
    if(lseek(fd, 1, SEEK_CUR) > endOfFile)
    {
        return -1;
    }

    // Se leggo meno byte della grandezza della porta ritorno errore
    if(read(fd, port, SIZE_OF_PORT) < SIZE_OF_PORT)
    {
        return -1;
    }

    // Salto di 1 posizione per non leggere '\n'
    lseek(fd, 1, SEEK_CUR);

    // Aggiungo il carattere terminatore alla fine della stringa
    port[SIZE_OF_PORT] = '\0';

    return 0;
}

// Variabili globali esclusivamente utilizzate dall'handler di SigInt
int globalSID;
int globalServerNumber;
int* globalWriteSock;
pthread_mutex_t* globalWriteMux;
void sigHand(int signo) {
    // Appena ricevo il segnale cerco di prendere il mutex di scrittura globale
    pthread_mutex_lock(globalWriteMux);

    // Ciclo su tutti i server per mandare il messaggio di terminazione
    for (int i = 0; i < globalServerNumber; ++i)
    {
        // Non mando il messaggio a me stesso
        if(i != globalSID)
        {
            sendInt(globalWriteSock[i], -1);
        }
    }
    pthread_mutex_unlock(globalWriteMux);

    // Chiudo il server
    printToMonitor("\nTerminazione del server corrente...\n");
    sleep(1);
    exit(0);
}

/**
 * Connette i vari server tra di loro
 * @param arg tStruct contenente tutte le info necessarie
 * @return socketDescriptor del server a cui ci si è connessi
 */
void* threadConnecter(void* arg)
{
    tConnect* tStruct = (tConnect*) arg;
    int* socketDescriptor = malloc(sizeof(int));
    char address[SIZE_OF_IP + 1], port[SIZE_OF_PORT + 1];

    /* Siccome vengono lanciati più threadConnecter parallamemnte potrebbero
     * esserci race condition sul cursore del file, quindi mi prendo il mutex */
    pthread_mutex_lock(tStruct->configMutex);
    int getRet = getIpAt(tStruct->row, *(tStruct->configFile), address, port);
    pthread_mutex_unlock(tStruct->configMutex);

    // Se getRet è 0 allora ha avuto successo ad ottenere indirizzo e porta
    if(getRet >= 0)
    {
        *socketDescriptor = connectToOtherServer(address, atoi(port));
        setNonBlocking(*socketDescriptor, true);

        // Mando il mio SID al server
        while(sendInt(*socketDescriptor, tStruct->sid) != 0)
        {
            sleep(1);
        }
    }
    else
    {
        printError("Errore getIpAt\n");
        *socketDescriptor = -1;
    }

    free(tStruct);
    return socketDescriptor;
}

/**
 * Gestisce la richiesta appena ricevuta dal client
 * @param arg tRequest contenente tutte le info necessarie
 * @return 0 il thread si chiude con pthread_exit
 */
void* threadClientRequest(void* arg) {
    tRequest* request = (tRequest*) arg;

    // La prima cosa che faccio è prendere il mutex client
    pthread_mutex_lock(request->clientMutex);

    // Inizio a leggere il commandCode dal client
    int commandCode;
    if(receiveInt(request->clientSocket, &commandCode) < 0)
    {
        printError("Si è verificato un errore durante la lettura di un int\n");
        exit(-1);
    }

    // Dichiaro le varie variabili e le inizializzo in base al comando
    int par1Length = -1, par2Length = -1;
    char *par1 = NULL, *par2 = NULL;

    // Se il comando è store, corrupt o search accetta almeno un parametro
    if(commandCode == 0 || commandCode == 1 || commandCode == 2)
    {
        if(receiveInt(request->clientSocket, &par1Length) < 0)
        {
            printError("Si è verificato un errore durante la lettura della lunghezza di un parametro\n");
            exit(-1);
        }

        if(receiveString(request->clientSocket, &par1, par1Length) < 0)
        {
            printError("Si è verificato un errore durante la lettura di una stringa\n");
            exit(-1);
        }

        // Se il comando è store o Corrupt prendo anche il secondo
        if(commandCode == 0 || commandCode == 1)
        {
            if(receiveInt(request->clientSocket, &par2Length) < 0)
            {
                printError("Si è verificato un errore durante la lettura della lunghezza del secondo parametro\n");
                exit(-1);
            }

            if(receiveString(request->clientSocket, &par2, par2Length) < 0)
            {
                printError("Si è verificato un errore durante la lettura della seconda stringa\n");
                exit(-1);
            }
        }
    }

    // Arrivato qui ho il codice ed i parametri, richiamo la funzione apposita in base al commandCode
    bool result;
    char* strSearch = NULL;

    switch (commandCode)
    {
        case 0: // Store
            // Provo ad effettuare lo store sulla lista corrente e mi salvo il risultato
            result = storeList(request->serverList, par1, par2);

            if(result)
            {
                // Se ha avuto successo procedo ad inoltrare a tutti i server, blocco il mutex di scrittura
                pthread_mutex_lock(request->writeMutex);

                /* Mando il messaggio RichiestaOrResult:SID:CommandCode:par1Lenght:par1:par2Lenght:par2
                 * a tutti i server */
                for (int i = 0; i < request->numServer; ++i)
                {
                    // Non mando il messaggio a me stesso
                    if(i != request->sid)
                    {
                        // Invio 0 perché sto richiedendo una connessione a quel server
                        sendInt(request->writeSockets[i], 0);

                        // Invio il resto delle informazioni necessarie
                        sendInt(request->writeSockets[i], request->sid);
                        sendInt(request->writeSockets[i], commandCode);
                        sendInt(request->writeSockets[i], par1Length);
                        sendString(request->writeSockets[i], par1, par1Length);
                        sendInt(request->writeSockets[i], par2Length);
                        sendString(request->writeSockets[i], par2, par2Length);
                    }
                }
                pthread_mutex_unlock(request->writeMutex);

                // Mi metto in attesa di ricevere TUTTE le risposte
                int resultReceived = 0;
                pthread_mutex_lock(request->resultStoreMutex);

                // Setto il primo valore, ovvero quello del server corrente
                request->resultStore[*request->resultStoreIndex] = 1;
                (*request->resultStoreIndex) += 1;
                resultReceived++;

                /* Metto il thread in wait finché non avrò ricevuto un numero
                 * di risultati pari al numero di server */
                while(resultReceived < request->numServer)
                {
                    resultReceived = 0;
                    pthread_cond_wait(request->condVar, request->resultStoreMutex);

                    /* Quando vengo risvegliato da una broadcast procedo a contare
                     * il numero di risultati nell'array dei risultati */
                    for (int i = 0; i < request->numServer; ++i)
                    {
                        if(request->resultStore[i] != 0)
                        {
                            resultReceived++;
                        }
                    }
                }

                /* Se sono uscito dal while allora ho i risultati, li scorro per verificarne l'esito
                 * mentre scorro svuoto anche le posizioni impostandole nuovamente a 0*/
                for (int j = 0; j < request->numServer; ++j)
                {
                    if(request->resultStore[j] == -1)
                    {
                        result = 0;
                    }

                    request->resultStore[j] = 0;
                }

                // Setto nuovamente l'indice a 0
                *(request->resultStoreIndex) = 0;

                pthread_mutex_unlock(request->resultStoreMutex);
            }

            /* Scrivo al client l'esito dell'operazione, può essere fallita direttamente
             * oppure a causa di un server che non è riuscito a fare la store */
            sendInt(request->clientSocket, result);

            free(par1);
            free(par2);
            break;

        case 1: // Corrupt
            result = corruptList(request->serverList, par1, par2);

            // Scrivo al client l'esito dell'operazione
            sendInt(request->clientSocket, result);

            free(par1);
            free(par2);
            break;

        case 2: // Search
            result = searchList(request->serverList, par1, &strSearch);
            int userResult;
            char* outputStr = NULL;

            // Se ha avuto successo procedo ad inoltrare a tutti i server, blocco il mutex di scrittura
            if(result)
            {
                pthread_mutex_lock(request->writeMutex);
                /* Mando il messaggio RichiestaOrResult:SID:CommandCode:par1Lenght:par1:par2Lenght:par2
                 * a tutti i server*/
                for (int i = 0; i < request->numServer; ++i)
                {
                    // Non mando il messaggio a me stesso
                    if(i != request->sid)
                    {
                        // Invio 0 perché sto richiedendo una connessione a quel server
                        sendInt(request->writeSockets[i], 0);

                        // Invio il resto delle informazioni necessarie
                        sendInt(request->writeSockets[i], request->sid);
                        sendInt(request->writeSockets[i], commandCode);
                        sendInt(request->writeSockets[i], par1Length);
                        sendString(request->writeSockets[i], par1, par1Length);
                    }
                }
                pthread_mutex_unlock(request->writeMutex);

                // Mi metto in attesa di ricevere TUTTE le risposte
                int resultReceived = 0;
                pthread_mutex_lock(request->resultSearchMutex);

                // Setto il primo valore, ovvero quello del server corrente
                request->resultSearch[*request->resultSearchIndex] = strSearch;
                (*request->resultSearchIndex) += 1;
                resultReceived++;
                
                /* Metto il thread in wait finché non avrò ricevuto un numero
                 * di risultati pari al numero di server */
                while(resultReceived < request->numServer)
                {
                    resultReceived = 0;
                    pthread_cond_wait(request->condVar, request->resultSearchMutex);

                    /* Quando vengo risvegliato da una broadcast procedo a contare
                     * il numero di risultati nell'array dei risultati */
                    for (int i = 0; i < request->numServer; ++i)
                    {
                        if(request->resultSearch[i] != NULL)
                        {
                            resultReceived++;
                        }
                    }
                }

                /* Ciclo tutti gli elementi, siccome la condizione è che y0 = y1 = ... = yN, dovranno
                 * necessariamente essere almeno tutti uguali all'elemento nella posizione y0 mentre
                 * scorro svuoto anche le posizioni e le reiposto a NULL, eccetto la posizione 0 */
                for (int i = 1; i < request->numServer; ++i)
                {
                    if(strcmp(request->resultSearch[0], request->resultSearch[i]) != 0) {
                        result = 0;
                    }

                    free(request->resultSearch[i]);
                    request->resultSearch[i] = NULL;
                }

                // Setto nuovamente l'indice a 0, salvo l'elemento in posizione 0 e setto la posizione a NULL
                *(request->resultSearchIndex) = 0;
                outputStr = request->resultSearch[0];
                request->resultSearch[0] = NULL;
                
                pthread_mutex_unlock(request->resultSearchMutex);

                /* Se arrivato qui result = 1, allora le coppie sono tutte uguali e userResult è 2,
                 * altrimenti 1 per ledger corrotto (un y è diverso da y0) */
                if(result) {
                    userResult = 2;
                }
                else
                {
                    userResult = 1;
                }
            }
            else
            {
                // La coppia non esiste, ritorno 0
                userResult = 0;
            }

            /* Scrivo al client l'esito dell'operazione, se userResult = 2 allora scrivo anche il valore di y*/
            sendInt(request->clientSocket, userResult);

            if(userResult == 2)
            {
                int strLen = strlen(outputStr) + 1;
                sendInt(request->clientSocket, strLen);
                sendString(request->clientSocket, outputStr, strLen);
                free(outputStr);
            }
            else if(userResult == 1) {
                free(outputStr);
            }

            free(par1);
            break;

        case 3: // List
            // Blocco il mutex della lista
            pthread_mutex_lock(request->serverList->mutex);

            // Scrivo il numero di elementi al client
            sendInt(request->clientSocket, request->serverList->dim);

            // Inizio a ciclare ed inviare i vari elementi
            Node* head = request->serverList->head;
            while(head != NULL)
            {
                int lengthX = strlen(head->key) + 1;
                int lengthY = strlen(head->value) + 1;

                sendInt(request->clientSocket, lengthX);
                sendString(request->clientSocket, head->key, lengthX);

                sendInt(request->clientSocket, lengthY);
                sendString(request->clientSocket, head->value, lengthY);

                head = head->next;
            }

            // Sblocco il mutex della lista
            pthread_mutex_unlock(request->serverList->mutex);
            break;

        default:
            // Non dovrei mai arrivare qui, ma se accade esco con errore
            printError("Errore: Comando dal client non valido\n");
            exit(-1);
    }

    // Dopo aver completato, libero il mutex
    pthread_mutex_unlock(request->clientMutex);

    // Chiudo la connessione con il client e libero la memoria
    close(request->clientSocket);
    free(arg);
    pthread_exit(NULL);
}

/**
 * Gestisce tutte le richieste provenienti da altri server, che siano di connessioni
 * oppure di risultati
 * @param arg struct tRequest
 * @return void
 */
void* threadReadHandler(void* arg) {
    tRequest* handler = (tRequest*) arg;

    // Faccio un ciclo infinito per rimanere sempre in attesa di messaggi
    while(true)
    {
        // Faccio il polling su tutti i server per vedere se mi hanno scritto
        int requestOrResult;
        for (int i = 0; i < handler->numServer; ++i)
        {
            // Se i == sid, starei ricevendo un messaggio da me stesso
            if(i != handler->sid)
            {
                // Se receiveInt mi restituisce 0 ho ricevuto un messaggio
                if(receiveInt(handler->readSockets[i], &requestOrResult) == 0)
                {
                    if(requestOrResult == -1) // Terminazione del server
                    {
                        printToMonitor("Un server è stato chiuso. Terminazione del server corrente...\n");
                        sleep(1);
                        exit(0);
                    }

                    if(requestOrResult == 0) // Nuova richiesta
                    {
                        // Creo il nuovo thread che si occuperà della richiesta del server
                        tRequestServer* requestServer = malloc(sizeof(tRequestServer));
                        requestServer->serverList = handler->serverList;
                        requestServer->serverMutex = handler->serverMutex;
                        requestServer->writeSockets = handler->writeSockets;
                        requestServer->writeMutex = handler->writeMutex;

                        // Ricevo il SID, prima o poi arriverà
                        while(receiveInt(handler->readSockets[i], &requestServer->otherSid) < 0);

                        // Ricevo il commandCode, può essere solo store (0) o search (2)
                        while(receiveInt(handler->readSockets[i], &requestServer->commandCode) < 0);

                        // Ricevo il par1
                        int strLen;
                        while(receiveInt(handler->readSockets[i], &strLen) < 0);
                        while(receiveString(handler->readSockets[i], &requestServer->par1, strLen) < 0);

                        // Se il comando è store prendo anche il secondo
                        if(requestServer->commandCode == 0)
                        {
                            while(receiveInt(handler->readSockets[i], &strLen) < 0);
                            while(receiveString(handler->readSockets[i], &requestServer->par2, strLen) < 0);
                        }

                        int tid;
                        pthread_create((pthread_t *) &tid, NULL, &threadServerRequest, requestServer);
                    }
                    else if(requestOrResult == 1) // Ricevi il risultato della store
                    {
                        int result;
                        while(receiveInt(handler->readSockets[i], &result) < 0);

                        pthread_mutex_lock(handler->resultStoreMutex);
                        if(result == 1)
                        {
                            handler->resultStore[*handler->resultStoreIndex] = 1;
                        }
                        else
                        {
                            handler->resultStore[*handler->resultStoreIndex] = -1;
                        }
                        (*handler->resultStoreIndex) += 1;
                        pthread_mutex_unlock(handler->resultStoreMutex);

                        // Invio il broadcast per svegliare i vari thread
                        pthread_cond_broadcast(handler->condVar);
                    }
                    else // Ricevi il risultato della search
                    {
                        int result;
                        while(receiveInt(handler->readSockets[i], &result) < 0);

                        pthread_mutex_lock(handler->resultSearchMutex);
                        // Se il valore letto è -1 allora la chiave non è presente
                        if(result == -1)
                        {
                            handler->resultSearch[*handler->resultSearchIndex] = malloc(sizeof(char) * NOT_FOUND_LENGTH);
                            strcpy(handler->resultSearch[*handler->resultSearchIndex], NOT_FOUND);
                        }
                        else
                        {
                            // Altrimenti salvo la stringa e la memorizzo nell'array dei risultati
                            char* strResult = NULL;
                            while(receiveString(handler->readSockets[i], &strResult, result) < 0);
                            handler->resultSearch[*handler->resultSearchIndex] = strResult;
                        }
                        (*handler->resultSearchIndex) += 1;

                        pthread_mutex_unlock(handler->resultSearchMutex);

                        // Invio il broadcast per svegliare i vari thread
                        pthread_cond_broadcast(handler->condVar);
                    }
                }
            }
        }
    }
}

/**
 * Gestisce le richieste provenienti da altri server, ovvero store o search
 * @param arg struct tRequestServer
 * @return void
 */
void* threadServerRequest(void* arg) {
    tRequestServer* reqServer = (tRequestServer*) arg;

    // La prima cosa che faccio è prendere il mutex server
    pthread_mutex_lock(reqServer->serverMutex);

    // Se il commandCode è 0 allora mi è stata richiesta la store
    if(reqServer->commandCode == 0)
    {
        // Provo ad effettuare lo store sulla lista corrente e mi salvo il risultato
        bool result = storeList(reqServer->serverList, reqServer->par1, reqServer->par2);

        // Prendo il mutex ed invio il risultato
        pthread_mutex_lock(reqServer->writeMutex);
        // Mando 1 per indicare che questo è il risultato della store al server
        sendInt(reqServer->writeSockets[reqServer->otherSid], 1);

        // Mando il risultato effettivo
        sendInt(reqServer->writeSockets[reqServer->otherSid], result);
        pthread_mutex_unlock(reqServer->writeMutex);

        free(reqServer->par2);
    }
    else
    {
        // Mi è stata richiesta una search
        char* value = NULL;
        bool result = searchList(reqServer->serverList, reqServer->par1, &value);

        pthread_mutex_lock(reqServer->writeMutex);
        // Mando 2 per indicare che questo è il risultato della search al server
        sendInt(reqServer->writeSockets[reqServer->otherSid], 2);

        if(result)
        {
            int strLenTot = strlen(value) + 1;
            sendInt(reqServer->writeSockets[reqServer->otherSid], strLenTot);
            sendString(reqServer->writeSockets[reqServer->otherSid], value, strLenTot);
            free(value);
        }
        else
        {
            sendInt(reqServer->writeSockets[reqServer->otherSid], -1);
        }
        pthread_mutex_unlock(reqServer->writeMutex);
    }

    // Rilascio il mutex
    pthread_mutex_unlock(reqServer->serverMutex);

    free(reqServer->par1);
    free(reqServer);
    pthread_exit(NULL);
}