#include "utilitySocket.h"

/* ************************************************************************ */

/**
 * Crea e connette il socket Server
 * @param address l'indirizzo in dotted notation del server
 * @param port la porta del server
 * @return socketDesacritpor se la creazione e connessione ha avuto successo, -1 altrimenti
 */
int createServerSocket(char* address, int port, int queueSize)
{
    // Creo il socket, lo imposto di rete (INET) e con connessione (STREAM)
    int socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);
    if(socketDescriptor < 0)
    {
        return -1;
    }

    // Creo l'indirizzo da assegnare al socket
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(port);
    inet_aton(address, &serverAddress.sin_addr); // Converte da dotted notation a network

    // Assegno l'indirizzo creato al socket
    if(bind(socketDescriptor, (const struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0)
    {
        close(socketDescriptor);
        return -1;
    }

    char clientName[INET_ADDRSTRLEN];
    if(inet_ntop(AF_INET, &serverAddress.sin_addr, clientName, INET_ADDRSTRLEN) == NULL)
    {
        perror("Errore inet_ntop");
        return -1;
    }

    char* printStr = malloc(sizeof(char) * MAX_STRING);
    sprintf(printStr, "Ho fatto il binding del socket sull'indirizzo %s:%d\n", clientName, port);
    printToMonitor(printStr);
    free(printStr);

    // Attivazione del servizio, imposto massimo queueSize clients
    if(listen(socketDescriptor, queueSize) < 0)
    {
        close(socketDescriptor);
        return -1;
    }

    return socketDescriptor;
}

/**
 * Crea e connette il socket Client
 * @param address l'indirizzo in dotted notation del server
 * @param port la porta del server
 * @return socketDesacritpor se la creazione e connessione ha avuto successo, -1 altrimenti
 */
int connectToOtherServer(char* address, int port)
{
    // Creo il socket, lo imposto di rete (INET) e con connessione (STREAM)
    int socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);
    if(socketDescriptor < 0)
    {
        return -1;
    }

    // Mi connetto al server, dichiaro un sockaddr del tipo del server
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(port);
    inet_aton(address, &serverAddress.sin_addr); // Converte da dotted notation a network e lo assegna

    // Ciclo finché non riesco a connettermi
    int loop;
    do {
        loop = connect(socketDescriptor, (const struct sockaddr *) &serverAddress, sizeof(serverAddress));
        sleep(1); // Ogni secondo riprovo a connettermi
    } while (loop < 0);

    return socketDescriptor;
}

/**
 * Imposta il fileDescriptor in ingresso in modalità bloccante o meno
 * @param fd il fileDescriptor
 * @param blocking valore booleano che indica se deve essere impostato o meno in modalità bloccante
 * @return true se ha avuto successo, false altrimenti
 **/
bool setNonBlocking(int fd, bool blocking) {
    // Prendo i flag attuali del fileDescriptor
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
    {
        return false;
    }

    // In base al valore booleano in ingresso modifico il flag
    if (blocking) {
        flags |= O_NONBLOCK;
    }
    else
    {
        flags &= ~O_NONBLOCK;
    }
    return fcntl(fd, F_SETFL, flags) != -1;
}

/**
 * Prende un intero dal socket
 * e della lettura dal socket che può essere minore della grandezza dell'intero
 * @param fd il file descriptor del socket su cui leggere
 * @param num il valore numerico che si intende leggere dal socket (è un parametro di ritorno)
 * @return 0 se ha successo, -1 altrimenti
 */
int receiveInt(int fd, int* num) {
    int32_t ret;
    char* data = (char*) &ret;
    int missingBytes = sizeof(ret); // Bytes mancanti
    int bytesRead;

    // Ciclo leggendo i bytes finché non leggo tutto l'intero
    do {
        bytesRead = read(fd, data, missingBytes);
        if(bytesRead < 0)
        {
            return -1;
        }

        // Sposto il puntatore per continuare a leggere da quel byte
        data += bytesRead;

        // Decremento il numero di bytes mancanti da ricevere
        missingBytes -= bytesRead;
    } while (missingBytes > 0);

    // Casto il risultato da network a valore host
    *num = ntohl(ret);
    return 0;
}

/**
 * Prende una stringa dal socket
 * @param fd il file descriptor del socket su cui leggere
 * @param str la stringa che si intende leggere dal socket (è un parametro di ritorno)
 * @param strLength la lunghezza della stringa compreso anche il terminatore
 * @return 0 se ha successo, -1 altrimenti
 */
int receiveString(int fd, char** str, int strLength) {
    char* uncompleteStr = malloc(sizeof(char) * strLength);
    *str = calloc(strLength, sizeof(char));    // Alloco la stringa
    int bytesToReceive = strLength;
    int bytesReceived;

    // Ciclo finché non ricevo tutti i bytes necessari
    do {
        bytesReceived = read(fd, uncompleteStr, bytesToReceive);
        if(bytesReceived < 0)
        {
            free(uncompleteStr);
            return -1;
        }

        // Copio quello che ho letto finora nella stringa completeStr
        uncompleteStr[bytesReceived - 1] = '\0';
        strcat(*str, uncompleteStr);

        // Decremento il numero di byte che mi mancano da ricevere
        bytesToReceive -= bytesReceived;
    } while(bytesToReceive > 0);

    free(uncompleteStr);
    return 0;
}

/**
 * Scrive un intero sul socket tenendo conto della differenza di rappresentazione di un int
 * @param fd il file descriptor del socket su cui scrivere
 * @param num il valore numerico che si intende scrivere sul socket
 * @return 0 se ha successo, -1 altrimenti
 */
int sendInt(int fd, int num) {
    int32_t conv = htonl(num);
    char* data = (char*) &conv;
    int missingBytes = sizeof(conv);
    int bytesWritten;

    // Ciclo scrivendo i bytes finché non scrivo tutto l'intero
    do {
        bytesWritten = write(fd, data, missingBytes);
        if (bytesWritten < 0)
        {
            return -1;
        }

        // Sposto il puntatore per continuare a scrivere da quel byte
        data += bytesWritten;

        // Decremento il numero di bytes mancanti da scrivere
        missingBytes -= bytesWritten;
    } while (missingBytes > 0);

    return 0;
}

/**
 * Scrive una stringa sul socket, necessaria in quanto una write può scrivere meno byte di quanto dovrebbe
 * @param fd il file descriptor del socket su cui scrivere
 * @param str la stringa che si intende scrivere sul socket
 * @param strLength la grandezza della stringa
 * @return 0 se ha successo, -1 altrimenti
 */
int sendString(int fd, char* str, int strLength) {
    // Bytes mancanti, si potrebbe usare strLenght ma una nuova variabile ne aumenta la leggibilità
    int missingBytes = strLength;
    int bytesWritten;

    // Ciclo scrivendo i bytes finché non scrivo tutto la stringa
    do {
        bytesWritten = write(fd, str, missingBytes);
        if (bytesWritten < 0)
        {
            return -1;
        }

        // Sposto il puntatore per continuare a scrivere da quel byte
        str += bytesWritten;

        // Decremento il numero di bytes mancanti da scrivere
        missingBytes -= bytesWritten;
    } while (missingBytes > 0);

    return 0;
}

/**
 * Scrive una stringa sullo standard output tramite funzioni non bufferizzate
 * @param message la stringa da stampare
 */
void printToMonitor(char* message) 
{
    sendString(STDOUT_FILENO, message, strlen(message));
}

/**
 * Scrive una stringa sullo standard error tramite funzioni non bufferizzate
 * @param message la stringa da stampare
 */
void printError(char* message)
{
    sendString(STDERR_FILENO, message, strlen(message));
}