#ifndef UTILITY_THREAD_H
#define UTILITY_THREAD_H

/* ************************************************************************ */

// Librerie
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "list.h"
#include "utilitySocket.h"

/* ************************************************************************ */

// Costanti
#define SIZE_OF_IP 15
#define SIZE_OF_PORT 4
#define QUEUE_SIZE 100
#define NOT_FOUND_LENGTH 1
#define NOT_FOUND "\0"

/* ************************************************************************ */

/**
 * Struct per i vari thread che si occupano delle connessioni in uscita verso gli altri server
 * @attribute sid il Server ID del server corrente
 * @attribute row riga su cui connettersi, rispetto al configFile, del singolo thread
 * @attribute configFile intero che punta al file di configurazione (statico)
 * @attribute configMutex puntatore ad un mutex comune a tutti i thread (statico)
 */
typedef struct tConnect
{
    int sid;
    int row;
    int* configFile;
    pthread_mutex_t* configMutex;
} tConnect;

/**
 * Struct per threadReadHandler e per le richieste del client
 * @attribute sid il Server ID del server corrente
 * @attribute clientSockets il socket del client per scrivere e ricevere messaggi
 * @attribute writeSockets indirizzo verso l'array dei socket di lettura
 * @attribute writeMutex indirizzo verso il mutex per il socket di scrittura
 * @attribute condVar indirizzo verso una condition variable per mettere in wait e risvegliare i thread
 * @attribute readSockets verso l'array dei socket di scrittura
 * @attribute numServer il numero di server totali
 * @attribute serverList la lista dinamica posseduta dal server corrente
 * @attribute resultStore indirizzo verso l'array di risultati della store
 * @attribute resultStoreIndex indirizzo verso l'indice dell'array dei risultati della store
 * @attribute resultStoreMutex indirizzo verso il mutex dell'array dei risultati della store
 * @attribute resultSearch indirizzo verso l'array di risultati della search
 * @attribute resultSearchIndex indirizzo verso l'indice dell'array dei risultati della search
 * @attribute resultSearchMutex indirizzo verso il mutex dell'array dei risultati della search
 * @attribute clientMutex indirizzo verso il mutex del client
 * @attribute serverMutex indirizzo verso il mutex del server
 */
typedef struct tRequest
{
    int sid;
    int clientSocket;
    int* writeSockets;
    pthread_mutex_t* writeMutex;
    pthread_cond_t* condVar;
    int* readSockets;
    int numServer;
    List* serverList;
    int* resultStore;
    int* resultStoreIndex;
    pthread_mutex_t* resultStoreMutex;
    char** resultSearch;
    int* resultSearchIndex;
    pthread_mutex_t* resultSearchMutex;
    pthread_mutex_t* clientMutex;
    pthread_mutex_t* serverMutex;
} tRequest;

/**
 * Struct per threadServerRequest per le richieste ricevute da altri server
 * @attribute otherSID il Server ID del server che si aspetta il risultato
 * @attribute commandCode il codice del comando che devo svolegere, 0 per store e 2 per search
 * @attribute par1 il primo parametro
 * @attribute par2 il secondo parametro
 * @attribute writeSockets indirizzo verso l'indice dell'array dei socket di scrittura
 * @attribute writeMutex indirizzo verso il mutex per l'array dei socket di scrittura
 * @attribute serverMutex indirizzo verso il mutex del server
 * @attribute serverList la lista dinamica posseduta dal server corrente
 */
typedef struct tRequestServer
{
    int otherSid;
    int commandCode;
    char* par1;
    char* par2;
    int* writeSockets;
    pthread_mutex_t* writeMutex;
    pthread_mutex_t* serverMutex;
    List* serverList;
} tRequestServer;

/* ************************************************************************ */

// Funzioni sul file di configurazione
int getIpAt(int, int, char*, char*);

// Funzioni relative ai threads
void* threadConnecter(void*);
void* threadClientRequest(void*);
void* threadServerRequest(void*);
void* threadReadHandler(void*);

/* ************************************************************************ */

#endif